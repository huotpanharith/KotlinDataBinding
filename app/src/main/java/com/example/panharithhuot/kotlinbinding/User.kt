package com.example.panharithhuot.kotlinbinding

/**
 * Created by panharithhuot on 9/14/17.
 */
data class User(val name: String, val age: Int)